package jp.co.junit4.sample;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConvertorUtilityTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		 System.out.println("setUpBeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDownAfterClass");
	}
	@Test
    public void nullToBlank_01() {
        System.out.println("nullToBlank_01");
        assertEquals("", ConvertorUtility.nullToBlank(null));
    }
    @Test
    public void nullToBlank_02() {
        System.out.println("nullToBlank_02");
        assertEquals("", ConvertorUtility.nullToBlank(""));
    }

    @Test
    public void nullToBlank_03() {
        System.out.println("nullToBlank_03");
        assertEquals("xyz", ConvertorUtility.nullToBlank("xyz"));
    }

    @Test
    public void concatHyphen_01() {
        System.out.println("concatHyphen_01");
        assertEquals("-", ConvertorUtility.concatHyphen(null, null));
    }

    /**
     * {@link jp.co.junit4.sample.ConvertorUtility#concatHyphen(java.lang.String, java.lang.String)}
     * のためのテスト・メソッド。(null+文字列)
     */
    @Test
    public void concatHyphen_02() {
        System.out.println("concatHyphen_02");
        assertEquals("-xyz", ConvertorUtility.concatHyphen(null, "xyz"));
    }

    /**
     * {@link jp.co.junit4.sample.ConvertorUtility#concatHyphen(java.lang.String, java.lang.String)}
     * のためのテスト・メソッド。(文字列+null)
     */
    @Test
    public void concatHyphen_03() {
        System.out.println("concatHyphen_03");
        assertEquals("abc-", ConvertorUtility.concatHyphen("abc", null));
    }

    /**
     * {@link jp.co.junit4.sample.ConvertorUtility#concatHyphen(java.lang.String, java.lang.String)}
     * のためのテスト・メソッド。(文字列+文字列)
     */
    @Test
    public void concatHyphen_04() {
        System.out.println("concatHyphen_04");
        assertEquals("abc-xyz", ConvertorUtility.concatHyphen("abc", "xyz"));
    }

/*
	@Test
	public void test() {
		fail("まだ実装されていません");
	}
*/
}
