package jp.co.junit4.sample;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FizzBuzzTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	@Before
	public void setUp() throws Exception {
		 System.out.println("setUp");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	@Test
    public void FizzBuzzTest_01() {
		assertEquals("Fizz",FizzBuzz.fizzBuzz(3));
    }

	@Test
    public void FizzBuzzTest_02() {
		assertEquals("Buzz",FizzBuzz.fizzBuzz(5));
    }

	@Test
    public void FizzBuzzTest_03() {
		assertEquals("FizzBuzz",FizzBuzz.fizzBuzz(15));
    }

	@Test
    public void FizzBuzzTest_04() {
		assertEquals("1",FizzBuzz.fizzBuzz(1));
    }
}
