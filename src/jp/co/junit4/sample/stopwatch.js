var timer;
var start;
var isStarted = false;

var startButton = document.getElementById('start');
var stopButton = document.getElementById('stop');
var resetButton = document.getElementById('reset');
var watch = document.querySelector('.stopwatch p');

startButton.addEventListener('click', watchStart, false);
stopButton.addEventListener('click', watchStop, false)
resetButton.addEventListener('click', watchReset, false);

function watchStart() {
	if(!isStarted) {
		start = new Date();
		timer = setInterval(updateWatch, 1000 / 60);

		isStarted = true;
	}
}


function watchStop() {
	if(isStarted) {
		clearInterval(timer);
		isStarted = false;
	}
}

function watchReset() {
	watchStop();
	watch.innerHTML = "00:00:00:000";
}

function updateWatch() {
	var date = new Date();
	var diff = date.getTime() - start.getTime();

	var hour = Math.floor(diff / 3600000);
	var minute = Math.floor(diff / 60000 % 60);
	var second = Math.floor(diff / 1000 % 60);
	var milliSecond = Math.floor(diff % 1000);

	if(hour < 10) {
		hour = "0" + hour;
	}

	if(minute < 10) {
		minute = "0" + minute;
	}

	if(second < 10) {
		second = "0" + second;
	}

	if(milliSecond < 100) {
		if(milliSecond < 10) {
			milliSecond = "00" + milliSecond;
		} else {
			milliSecond = "0" + milliSecond
		}
	}

	watch.innerHTML = hour + ':' + minute + ':' + second + ':'+ milliSecond;

}