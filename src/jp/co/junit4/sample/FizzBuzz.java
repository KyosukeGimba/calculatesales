package jp.co.junit4.sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FizzBuzz {
	public static void main(String[] args) throws IOException {

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		int num = Integer.parseInt(br.readLine());
		System.out.println(fizzBuzz(num));
	}

	public static String fizzBuzz(int num) {
		String str;
		if(num % 3 == 0 && num % 5 == 0) {
			str = "FizzBuzz";
			return str;
		} else if(num % 5 == 0) {
			str = "Buzz";
			return str;
		} else if(num % 3 == 0) {
			str = "Fizz";
			return str;
		} else
			str = String.valueOf(num);
		return str;
	}
}

