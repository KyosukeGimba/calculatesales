package jp.alhinc.gimba_kyosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CalculateSales {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> salesMap = new HashMap<String, String>();
		Map<String, Long> summap = new HashMap<String,Long>();

		if(!sFileReader(args[0], "branch.lst",salesMap,summap)) {
			return;
		}
		File dir = new File(args[0]);
		File[] files = dir.listFiles();
		String number;
		ArrayList<File> rcdFiles  = new ArrayList<>(0);
		BufferedReader br1 = null;

		for(int i = 0; i < files.length; i++) 	{
			if((files[i].getName()).matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
				rcdFiles.add(files[i]);
			}
		}
		for( int i = 0 ; i < rcdFiles.size() - 1; i++){
			int beforefile = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int afterfile = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0, 8));
			if (afterfile - beforefile != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int i = 0; i< rcdFiles.size(); i++){
			try {
				ArrayList<String> saleslist = new ArrayList<>();
				FileReader sales = new FileReader(rcdFiles.get(i));
				br1 = new BufferedReader(sales);
				while((number = br1.readLine()) !=null) {
					saleslist.add(number);
				}
				if(saleslist.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				if(!saleslist.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				if(!summap.containsKey(saleslist.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				Long aftersales = summap.get(saleslist.get(0)) + Long.parseLong(saleslist.get(1));
				summap.put(saleslist.get(0), aftersales);
				if(10 < String.valueOf(aftersales).length()) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br1 != null) {
					try {
						br1.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(sFileWriter(args[0], "branch.out", salesMap, summap)) {
			return;
		}
	}
	static boolean sFileReader(String calculatedir, String filename, HashMap<String,String> salesMap, Map<String, Long> summap) {
		BufferedReader br = null;
		try {
			File file = new File(calculatedir,filename);
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) !=null) {
				String[] items = line.split(",");
				if (items.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				if(!items[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				salesMap.put(items[0],items[1]);
				summap.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
				}
			}
		}
		return true;
	}
	static boolean sFileWriter(String calculatedir, String filename, HashMap<String,String> salesMap, Map<String, Long> summap)  {
		BufferedWriter salessales = null;
		try{
			File sumsum = new File(calculatedir,filename);
			sumsum.createNewFile();

			FileWriter sumsales;
			sumsales = new FileWriter(sumsum);
			salessales = new BufferedWriter(sumsales);

			for(Map.Entry<String, Long> entry : summap. entrySet()) {
				salesMap.get(entry.getKey());
				String branchcode = entry.getKey();
				String branchname = salesMap.get(entry.getKey());
				Long branchsales = entry.getValue();
				salessales.write((branchcode + "," + branchname + "," + branchsales));
				salessales.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(salessales != null) {
				try {
					salessales.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
